(* Prior to the MPRI 2.7.1 and 2.7.2 courses, I did not have any experience
 with Coq, except for an exercise-session-short introduction as part of ENS
 Paris's Semantics and applications to verification bachelor-level course.

 This project proved to be fairly pleasant to carry out as Coq's documentation
 was enough to find the missing pieces I was needing, in particular for part 2,
 and the proofs followed from my intuition and did not provide any major
 setback.

 I still think my code could still be improved in terms of readability. I tried
 factoring it as much as possible, though I would have liked to do more.
 In particular, the pick_hyp_size_invariant and soundness_pick_hyp lemmas could
 both have been deduced from the fact that the elements of the output of
 pick_hyp may be concatenated into permutations of its input. I was originally
 following this strategy but directly proving the two lemmas ended up being much
 more efficient than using the Permutation library. This choice makes the code
 less scalable.
 I would also have liked to find a way to destruct Forall properties in a single
 command instead of having to repeatedly apply Forall_nil and Forall_cons.
 Finally, I found that simplifying the code after proving a result turned out to
 be fairly non-trivial as there are many ways to write the same sequence of
 tactics but no canonical way to do so, and as a compromise has to be found
 between aggressively factoring the code, especially using indices of the
 subgoals, and keeping it readable, in the sense that it should be
 understandable without running it with Coq. *)


(* 2. Implementing the decision procedure *)
Section Implementation.
  Context (A B C : Prop).
  (* 2.2. Building the tactic *)
  (* Question 1 *)
  Ltac tauto1 :=
    let rec tauto_with depth :=
      let start goal := idtac "[" depth "]" "proving" goal in
      let rec tauto_next := tauto_with (S depth) in
      match goal with
      (* Ax *)
      | H : ?A |- ?A =>
          idtac "[" depth "]" "exact" H ":" A ;
          exact H
      (* False-E *)
      | H : False |- ?A =>
          idtac "[" depth "]" "apply False_ind" A H ;
          apply (False_ind A H)
      (* True-I *)
      | |- True =>
          idtac "[" depth "]" "exact I" ;
          exact I

      (* /\-E *)
      | H : ?A /\ ?B |- _ =>
          let Hl := fresh in
          let Hr := fresh in
          idtac "[" depth "]" "destruct" H ":" A "/\" B "as" "[" Hl Hr "]" ;
          destruct H as [ Hl Hr ] ;
          tauto_next
      (* \/-E *)
      | H : ?A \/ ?B |- _ =>
          case H ;
          clear H ;
          [ idtac "[" depth "]" "case" A ; tauto_next
          | idtac "[" depth "]" "case" B ; tauto_next ]
      (* /\-I *)
      | |- ?A /\ ?B =>
          idtac "[" depth "]" "split" ;
          split ;
          [ start A ; tauto_next
          | start B ; tauto_next ]
      (* \/-I *)
      | |- ?A \/ ?B =>
          (idtac "[" depth "]" "left" ; left ; start A ; tauto_next)
          || (idtac "[" depth "]" "right" ; right ; start B ; tauto_next)
      (* =>-I *)
      | |- ?A -> ?B =>
          let H := fresh in
          idtac "[" depth "]" "intro" H ":" A ;
          intro H ;
          start B ;
          tauto_next

      (* we use this as a last resort as it may apply in a lot of contexts *)
      (* =>-E *)
      | H : ?A -> ?B |- ?B =>
          idtac "[" depth "]" "apply" H ":" A "->" B ;
          apply H ;
          start A ;
          clear H ;
          tauto_next
      (* cut *)
      | H : ?A -> ?B |- ?C =>
          idtac "[" depth "]" "cut" C "using" H ":" A "->" B ;
          cut B ;
          [ start (B -> C) ; clear H ; tauto_next
          | start A ; apply H ; clear H ; tauto_next ]

      | |- ?A => idtac "[" depth "]" "failed to prove" A ; fail 0
      end
    in try tauto_with 0.

  (* Question 2 *)
  Goal False -> A.
    tauto1.
  Qed.

  Goal A /\ B -> A.
    tauto1.
  Qed.

  Goal A /\ B -> B.
    tauto1.
  Qed.

  Goal A /\ B -> B /\ A.
    tauto1.
  Qed.

  Goal A -> A \/ B.
    tauto1.
  Qed.

  Goal B -> A \/ B.
    tauto1.
  Qed.

  Goal (A -> C) -> (B -> C) -> (A \/ B) -> C.
    tauto1.
  Qed.

  Goal A -> (A -> B) -> B.
    tauto1.
  Qed.

  Goal A -> (A -> B) -> (B -> C) -> B.
    tauto1.
  Qed.

  Goal A -> (A -> B) -> (B -> C) -> C.
    tauto1.
  Qed.

  (* additional tests *)
  Goal (A -> (A /\ B)) -> A -> B.
    tauto1.
  Qed.


  (* 2.3. Backtrack control *)
  (* Question 1 *)
  Ltac tauto2 :=
    let rec tauto_with depth :=
      let start goal := idtac "[" depth "]" "proving" goal in
      let rec cancel_backtrack := idtac "[" depth "]" "not backtracking" ; fail 1 in
      let rec tauto_next := tauto_with (S depth) in
      let rec tauto_iff := tryif tauto_next then idtac else cancel_backtrack in
      match goal with
      (* Ax *)
      | H : ?A |- ?A =>
          idtac "[" depth "]" "exact" H ":" A ;
          exact H
      (* False-E *)
      | H : False |- ?A =>
          idtac "[" depth "]" "apply False_ind" A H ;
          apply (False_ind A H)
      (* True-I *)
      | |- True =>
          idtac "[" depth "]" "exact I" ;
          exact I

      (* /\-E *)
      | H : ?A /\ ?B |- _ =>
          let Hl := fresh in
          let Hr := fresh in
          idtac "[" depth "]" "destruct" H ":" A "/\" B "as" "[" Hl Hr "]" ;
          destruct H as [ Hl Hr ] ;
          tauto_iff
      (* \/-E *)
      | H : ?A \/ ?B |- _ =>
          case H ;
          clear H ;
          [ idtac "[" depth "]" "case" A ; tauto_iff
          | idtac "[" depth "]" "case" B ; tauto_iff ]
      (* /\-I *)
      | |- ?A /\ ?B =>
          idtac "[" depth "]" "split" ;
          split ;
          [ start A ; tauto_iff
          | start B ; tauto_iff ]
      (* \/-I *)
      | |- ?A \/ ?B =>
          tryif
            (idtac "[" depth "]" "left" ; left ; start A ; tauto_next)
            || (idtac "[" depth "]" "right" ; right ; start B ; tauto_next)
          then idtac else cancel_backtrack
      (* =>-I *)
      | |- ?A -> ?B =>
          let H := fresh in
          idtac "[" depth "]" "intro" H ":" A ;
          intro H ;
          start B ;
          tauto_iff

      (* we use this as a last resort as it may apply in a lot of contexts *)
      (* =>-E *)
      | H : ?A -> ?B |- ?B =>
          idtac "[" depth "]" "apply" H ":" A "->" B ;
          apply H ;
          start A ;
          clear H ;
          tauto_next
      (* cut *)
      | H : ?A -> ?B |- ?C =>
          idtac "[" depth "]" "cut" C "using" H ":" A "->" B ;
          cut B ;
          [ start (B -> C) ; clear H ; tauto_next
          | start A ; apply H ; clear H ; tauto_next ]

      | |- ?A => idtac "[" depth "]" "failed to prove" A ; fail 0
      end
    in try tauto_with 0.

  Goal False -> A.
    tauto2.
  Qed.

  Goal A /\ B -> A.
    tauto2.
  Qed.

  Goal A /\ B -> B.
    tauto2.
  Qed.

  Goal A /\ B -> B /\ A.
    tauto2.
  Qed.

  Goal A -> A \/ B.
    tauto2.
  Qed.

  Goal B -> A \/ B.
    tauto2.
  Qed.

  Goal (A -> C) -> (B -> C) -> (A \/ B) -> C.
    tauto2.
  Qed.

  Goal A -> (A -> B) -> B.
    tauto2.
  Qed.

  Goal A -> (A -> B) -> (B -> C) -> B.
    tauto2.
  Qed.

  Goal A -> (A -> B) -> (B -> C) -> C.
    tauto2.
  Qed.

  Goal (A -> (A /\ B)) -> A -> B.
    tauto2.
  Qed.

  (* Question 2 *)
  Goal (A /\ B) -> False.
    tauto1.
    tauto2.
  Abort.

  Goal A /\ (B -> C) -> (False /\ A) \/ A.
    tauto1.
    Restart.
    tauto2.
  Qed.
End Implementation.


(* 3. Formalizing the tactic *)
Require Import String.
Require Import List.
Import ListNotations.
Require Import Permutation.
Require Import Lia.
Section Formalization.

  (* 3.1. Tactic steps *)
  (* Question 1 *)

  Inductive form : Type :=
  | F | T : form
  | Var : string -> form
  | And : form -> form -> form
  | Or : form -> form -> form
  | Imply : form -> form -> form.

  Definition A := Var "A".
  Definition B := Var "B".
  Definition C := Var "C".
  Definition D := Var "D".

  Notation "A /\ B" := (And A B).
  Notation "A \/ B" := (Or A B).
  Notation "A -> B" := (Imply A B).
  (* when the notation is ambiguous *)
  Delimit Scope type_scope with t.

  Fixpoint form_eq (f1 f2 : form) : bool :=
    match (f1, f2) with
    | (F, F) | (T, T) => true
    | (Var x, Var y) => match string_dec x y with
                       | left _ => true
                       | right _ => false
                       end
    | (A /\ B, C /\ D)
    | (A \/ B, C \/ D)
    | (A -> B, C -> D) => form_eq A C && form_eq B D
    | _ => false
    end.

  Record seq : Type := { hyp : list form ; goal : form }.
  Definition seq_of_goal (f : form) : seq := {| hyp := nil ; goal := f |}.
  Coercion seq_of_goal : form >-> seq.

  (* Question 2 *)
  Definition is_leaf (s : seq) : bool :=
    match goal s with
    | T => true
    | f => existsb (form_eq f) (hyp s)
    end.

  Compute is_leaf {| hyp := [ A ; B ; C ] ; goal := T |}.
  Compute is_leaf {| hyp := [ A ; B ; C ] ; goal := B |}.
  Compute is_leaf {| hyp := [ A ; B ; F ] ; goal := C |}.

  (* Question 3 *)
  Definition subgoals := list (list seq).

  Fixpoint pick_hyp (hyps : list form) : list (form * list form) :=
    match hyps with
    | [] => []
    | f :: fs => (f, fs) :: map (fun picked_others => (fst picked_others, f :: snd picked_others)) (pick_hyp fs)
    end.

  Definition replace_hyp s new_hyp := {| hyp := new_hyp ; goal := goal s |}.
  Definition replace_goal s new_goal := {| hyp := hyp s ; goal := new_goal |}.
  Definition elim s :=
    map (fun hyp_others => match hyp_others with
    | (A \/ B, others) => [ replace_hyp s (A :: others) ;
                          replace_hyp s (B :: others) ]
    | (A /\ B, others) => [ replace_hyp s (A :: B :: others) ]
    | (A -> B, others) => [ replace_hyp s (B :: others) ;
                          {| hyp := others ; goal := A |} ]
    | _ => []
    end) (pick_hyp (hyp s)).
  Definition intro s :=
    match goal s with
      | A \/ B => [ [ replace_goal s A ] ;
                  [ replace_goal s B ] ]
      | A /\ B => [ [ replace_goal s A ; replace_goal s B ] ]
      | A -> B => [ [ {| hyp := A :: (hyp s) ; goal := B |} ] ]
      | _ => []
    end.

  Definition step (s : seq) : subgoals :=
    filter (fun premises => match premises with
                         | [] => false
                         | _ => true
                         end)
           (intro s ++ elim s).

  Compute step {| hyp := [ A \/ B ] ; goal := C -> D |}.

  (* Question 4 *)
  Fixpoint tauto (n : nat) (s : seq) : bool :=
    match n with
    | 0 => false
    | S n' => is_leaf s || existsb (forallb (tauto n')) (step s)
    end.

  Compute tauto 2 (A -> F).
  Compute tauto 3 ((A /\ B) -> A).
  Compute tauto 3 ((A /\ B) -> B).
  Compute tauto 3 ((A /\ B) -> (B /\ A)).
  Compute tauto 3 ((A \/ B) -> A).
  Compute tauto 3 ((A \/ B) -> B).
  Compute tauto 5 ((A -> C) -> (B -> C) -> (A \/ B) -> C).
  Compute tauto 2 (A -> (A -> B) -> B).
  Compute tauto 2 (A -> (A -> B) -> (B -> C) -> B).
  Compute tauto 2 (A -> (A -> B) -> (B -> C) -> C).
  (* additional tests *)
  Compute tauto 3 ((A -> (A /\ B)) -> A -> B).


  (* 3.2. Termination *)
  (* Question 1 *)
  Fixpoint length (f : form) : nat :=
    match f with
    | F | T | Var _ => 1
    | A -> B | A /\ B | A \/ B => S (length A + length B)
    end.

  Definition size_hyps (hyps : list form) :=
    list_sum (map length hyps).
  Definition size (s : seq) : nat :=
    length (goal s) + size_hyps (hyp s).

  (* Question 2 *)
  Lemma pick_hyp_size_invariant : forall hyps : list form,
      Forall (fun picked_others => length (fst picked_others)
                                + size_hyps (snd picked_others)
                                = size_hyps hyps)
             (pick_hyp hyps).
  Proof.
    induction hyps as [| f fs ].
    - simpl. trivial.
    - apply Forall_cons.
      + trivial.
      + apply Forall_map.
        refine (Forall_impl _ _ IHfs).
        unfold size_hyps.
        intros [ picked others ].
        simpl.
        intros.
        rewrite <- H.
        lia.
  Qed.

  Theorem step_size_variant : forall s : seq, Forall (Forall (fun premise => size premise < size s)) (step s).
    intros.
    apply incl_Forall with (l1 := intro s ++ elim s).
    apply incl_filter.
    apply Forall_app.
    unfold size.
    split ;
      destruct s ;
      [ (* case intro *)
        destruct goal0
      | (* case elim *)
        apply Forall_map ;
        refine (Forall_impl _ _ (pick_hyp_size_invariant hyp0)) ;
        intros [ picked others ] ;
        simpl ;
        intro H ;
        destruct picked ] ;
      repeat (apply Forall_cons || apply Forall_nil) ;
      try rewrite <- H ;
      unfold size_hyps ;
      try rewrite map_cons ;
      simpl ;
      lia.
  Qed.


  (* 3.3. Soundness *)
  (* Question 1 *)
  Context (prop_of_string : string -> Prop).

  Fixpoint sem (f : form) : Prop :=
    match f with
    | F => False
    | T => True
    | Var ident => prop_of_string ident
    | A /\ B => sem A /\ sem B
    | A \/ B => sem A \/ sem B
    | A -> B => sem A -> sem B
    end.

  Definition valid (s : seq) : Prop :=
    Forall sem (hyp s) -> sem (goal s).
  Definition valid_subgoals (sg : subgoals) : Prop :=
    Exists (Forall valid )%t sg.

  (* Question 2 *)
  Lemma form_eq_iff_eq : forall f1 f2 : form, form_eq f1 f2 = true <-> f1 = f2.
  Proof.
    induction f1, f2 ;
      split ;
      simpl.
    (* case Var s *)
    29-30 : destruct (string_dec s s0) ; intro H.
    all : try (easy
              || (intro ; exfalso ; now apply Bool.diff_false_true)).
    (* case Var s = Var s0 with s = s9 *)
    now rewrite e.
    (* case Var s = Var s0 with s <> s0 *)
    injection H ;
      intro ;
      exfalso ;
      now apply n.
    (* form_eq _ _ = true -> _ = _ *)
    1, 3, 5: intro H ;
    apply Bool.andb_true_iff in H ;
    destruct H ;
    apply IHf1_1 in H ;
    apply IHf1_2 in H0 ;
    now rewrite H, H0.
    (* _ = _ -> form_eq _ _ = true *)
    all : intro ;
      apply Bool.andb_true_iff ;
      injection H ;
      intros ;
      now split ;
      [ apply IHf1_1 | apply IHf1_2 ].
  Qed.

  Proposition soundness_leaf : forall s : seq, is_leaf s = true -> valid s.
  Proof.
    unfold is_leaf, valid ;
      destruct s, goal0 ;
      simpl ;
      intros exists_goal_in_hyp forall_sem_hyp.
    (* case T *)
    2 : trivial.
    all : apply existsb_exists in exists_goal_in_hyp ;
      destruct exists_goal_in_hyp as [ f [ f_in_hyp0 f_eq ] ] ;
      apply (Forall_forall sem hyp0) in f_in_hyp0 ;
      [ destruct f ;
        try (exfalso ; now apply Bool.diff_false_true)
      | easy ].
    (* case F *)
    trivial.
    (* case Var s *)
    now destruct (string_dec s s0) ;
      [ rewrite e
      | exfalso ; apply Bool.diff_false_true].
    (* case A binop B *)
    all : apply Bool.andb_true_iff in f_eq ;
      destruct f_eq as [ f_eq1 f_eq2 ] ;
      apply form_eq_iff_eq in f_eq1, f_eq2 ;
      now rewrite f_eq1, f_eq2.
  Qed.

  (* Question 3 *)
  Lemma soundness_pick_hyp : forall hyps : list form,
      Forall (fun picked_others : form * list form =>
                  (Forall sem hyps -> sem (fst picked_others) /\ Forall sem (snd picked_others))%t)
             (pick_hyp hyps).
  Proof.
    intros.
    induction hyps as [| f fs ].
    - apply Forall_nil.
    - apply Forall_cons.
      + simpl.
        intro forall_sem_f_fs.
        apply Forall_cons_iff in forall_sem_f_fs.
        trivial.
      + apply Forall_map.
        refine (Forall_impl _ _ IHfs).
        intros [ picked others ].
        simpl.
        intros IH forall_sem_f_fs.
        apply Forall_cons_iff in forall_sem_f_fs.
        destruct forall_sem_f_fs as [ sem_f forall_sem_fs ].
        apply IH in forall_sem_fs as [ sem_picked forall_sem_others ].
        auto.
  Qed.

  Proposition soundness_step : forall s : seq, valid_subgoals (step s) -> valid s.
  Proof.
    destruct s.
    intros valid_subgoals_step forall_sem_hyp.
    simpl.
    simpl in forall_sem_hyp.
    apply Exists_exists in valid_subgoals_step.
    destruct valid_subgoals_step as [ premises [ premises_in_step forall_valid_premises ] ].
    apply filter_In in premises_in_step as [ premises_in_step premises_not_empty ].
    apply in_app_or in premises_in_step.
    destruct premises_in_step as [ premises_in_intro | premises_in_elim ].
    (* case intro *)
    - destruct goal0 ;
        simpl ;
        simpl in premises_in_intro ;
        repeat (let H := fresh in destruct premises_in_intro as [ H | premises_in_intro ]).
      (* premises in [[]] *)
      1-3, 5, 8, 10 : now exfalso.
      all : rewrite <- H in forall_valid_premises ;
        repeat (apply Forall_cons_iff in forall_valid_premises ;
                let H := fresh in destruct forall_valid_premises as [ H forall_valid_premises ]) ;
        eauto using Forall_cons.
    (* case elim *)
    - unfold elim in premises_in_elim.
      simpl in premises_in_elim.
      apply in_map_iff in premises_in_elim.
      destruct premises_in_elim as [ [ picked others ] [ premises_eq picked_others_in_pick_hyp ] ].
      apply (ZifyClasses.rew_iff _ _ (Forall_forall _ _) (soundness_pick_hyp hyp0))
        in picked_others_in_pick_hyp as [ sem_picked forall_sem_others ].
      + simpl in sem_picked, forall_sem_others.
        rewrite <- premises_eq in forall_valid_premises, premises_not_empty.
        destruct picked.
        (* premises in [[]] *)
        1-3 : exfalso ;
        auto using Bool.diff_false_true.
        1-3 : unfold valid, replace_hyp in forall_valid_premises ;
        simpl in forall_valid_premises, sem_picked ;
        simpl ;
        repeat (apply Forall_cons_iff in forall_valid_premises ;
                let H := fresh in destruct forall_valid_premises as [ H forall_valid_premises]).
        1-2 : destruct sem_picked.
        all : eauto using Forall_cons.
      + trivial.
  Qed.

  (* Question 4 *)
  Theorem soundness : forall (n : nat) (s : seq), tauto n s = true -> valid s.
  Proof.
    induction n ;
      simpl ;
      intros.
    - exfalso.
      now apply Bool.diff_false_true.
    - apply Bool.orb_true_iff in H.
      destruct H.
      + now apply soundness_leaf.
      + apply soundness_step.
        unfold valid_subgoals.
        apply Exists_exists.
        apply existsb_exists in H.
        destruct H as [ premises [ premises_in_step_s forall_tauto_n_premises ] ].
        exists premises.
        split.
        * trivial.
        * apply Forall_forall.
          intros.
          apply IHn.
          now apply (forallb_forall (tauto n) premises).
  Qed.
End Formalization.
